module RedmineGantt
  module Hooks
    class ViewLayoutsBaseHtmlHeadHook < Redmine::Hook::ViewListener
      def view_layouts_base_html_head(context={})
        if context[:controller] && context[:controller].is_a?(GanttController)
          return stylesheet_link_tag("dhtmlxgantt.css", :plugin => "redmine_gantt", :media => "screen") +
          stylesheet_link_tag("gantt.css", :plugin => "redmine_gantt", :media => "screen") +
          javascript_include_tag('gantt.js', :plugin => 'redmine_gantt')+
          javascript_include_tag('dhtmlxgantt.js', :plugin => 'redmine_gantt') + 
          javascript_include_tag('locale_ru.js', :plugin => 'redmine_gantt') + 
          javascript_include_tag('https://export.dhtmlx.com/gantt/api.js')
        else
          return ''
        end
      end
    end
  end
end


require 'redmine'
require_dependency 'hooks/view_layouts_base_html_head_hook.rb'
Redmine::Plugin.register :redmine_gantt do
	name 'Redmine Gaant plugin'
	author 'Ilya Buzlov'
	description 'This is a plugin use gant http://dhtmlx.com/docs/products/dhtmlxGantt/'
	version '0.0.1'
	author_url 'http://vk.com/ionipersii'

  permission :redmine_gantt, { :gantt => [:index] }, :public => true
  menu :project_menu,:redmine_gantt,{:controller => 'gantt',:action => 'index'},:caption => "Gantt"
end

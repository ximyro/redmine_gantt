$(document).ready(function(){
	gantt.config.xml_date = "%Y-%m-%d"; 
	gantt.config.columns = [
	{name:"text",       label:"Задача",  width:"*", tree:true },
	{name:"start_date", label:"Время начала", align: "center" },
	{name:"duration",   label:"Длительность",   align: "center" }
	];
	gantt.templates.scale_cell_class = function(date){
		if(date.getDay()==0||date.getDay()==6){
			return "weekend";
		}
	};
	gantt.templates.task_cell_class = function(item,date){
		if(date.getDay()==0||date.getDay()==6){ 
			return "weekend" ;
		}
	};
	gantt.init("gantt_here");   
	$.ajax({
		url: "/issues.json",
		data: {
			project_id: current_project_id,
			key: current_key,
			include: "relations"
		}
	}).done(function(data){
		var links = [];
		$.each(data.issues,function(i,issue){
			$.each(issue.relations,function(j,relation){
				links.push({
					id: relation.id,
					source: relation.issue_id,
					target: relation.issue_to_id,
					type: "0"
				});
			});
		});
		var tasks = {
			data: data.issues.map(function(issue){
				duration = 1
				if (issue.due_date){
					var due_date = new Date(issue.due_date);
					var start_date = new Date(issue.start_date);
					var diff = due_date.getTime() - start_date.getTime();
					duration = Math.floor(diff / (1000 * 60 * 60 * 24));
				}
				return {
					id: issue.id,
					text: issue.subject,
					start_date: issue.start_date,
					duration: duration,
					progress: issue.done_ratio/100,
					open: issue.status.id == 9,
				}
			}),
			links: links
		};
		gantt.parse(tasks);
		gantt.attachEvent("onTaskClick", function(id, e) {
			return false;
		});
		gantt.attachEvent("onTaskDblClick", function(id, e) {
			return false;
		});
		gantt.attachEvent("onBeforeLinkAdd", function(id, link){
			if (link.type == 0){
				var sourceTask = gantt.getTask(link.source);
				var targetTask = gantt.getTask(link.target);
				if (sourceTask.end_date >= targetTask.start_date){
					alert("Неправильно созданна ссылка")
					return false;
				}
			}
			$.ajax({
				url: "/issues/" + link.source + "/relations.json",
				type: "POST",
				data: {
					key: current_key,
					relation: {
						issue_to_id: link.target,
						relation_type: "precedes"
					}
				}
			}).done(function(data){
				link.id = data.relation.id;
				gantt.refreshLink(id);  
			}).fail(function(data){
				dhtmlx.message($.parseJSON(data.responseText).errors[0]);
			});
		});
		gantt.attachEvent("onLinkClick", function(id,e){
			if (confirm("Вы уверенны, что хотите удалить связь?")){
				$.ajax({
					url: "/relations/" + id + ".json",
					type: "DELETE",
					data: {
						key: current_key,
					}
				}).done(function(){
				}).fail(function(data){
					if (data.responseText){
						dhtmlx.message($.parseJSON(data.responseText).errors[0]);
					}else{
						dhtmlx.message("Ссылка успешно удалена");
						gantt.deleteLink(id);
					}
				});
			}
		});
		gantt.attachEvent("onBeforeTaskChanged", function(id, mode, old_task){
			var task = gantt.getTask(id);
			$.ajax({
				url: "/issues/" + id + ".json",
				type: "PUT",
				data: {
					key: current_key,
					issue: {
						start_date: task.start_date.toLocaleFormat("%Y-%m-%d"),
						due_date: task.end_date.toLocaleFormat("%Y-%m-%d")
					}
				}
			}).fail(function(data){
				if (data.responseText){
					dhtmlx.message($.parseJSON(data.responseText).errors[0]);
				}else{
					dhtmlx.message("Задача " + task.text + " успешно обновлена");
				}
			});
			return true;
		});
	});
$(".gantt-export-excel").click(function(){
	gantt.exportToExcel({
		locale:"ru",
	})
});
$(".gantt-export-png").click(function(){
	gantt.exportToPNG({
		locale:"ru",
	});
});
$(".gantt-export-pdf").click(function(){
	gantt.exportToPDF({
		locale:"ru",
	});
});
});
